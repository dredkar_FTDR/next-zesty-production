export default function HomePage({albums}) {
    const albumLi = albums.data.map((album) => {
           return <li key={album.id}>{album.title}</li>
    })
    return (
        <ul>
            {albumLi}
        </ul>
    )
}

export async function getStaticProps(context) {
    const res = await fetch(`${process.env.CMS_URL}/albums.json`)
    const data = await res.json()

    if (!data) {
      return {
        notFound: true,
      }
    }

    return {
       props: {
          albums: data
       }, 
       revalidate: 1
    }
  }